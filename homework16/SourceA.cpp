#include "sqlite3.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

unordered_map<string, vector<string>> results;
void clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

void printTable()
{
	auto iter = results.end();
	iter--;
	//int size = results.begin()->second.size();
	int size = iter->second.size();
	size = results.begin()->second.size();
	for (int i = -2; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			if (i == -2)
			{
				//cout << it->first << " ";
				printf("|%*s|", 13, it->first.c_str());
			}
			else if (i == -1)
				cout << "_______________";
			else
				//	cout << it->second.at(i) << " ";
				printf("|%*s|", 13, it->second.at(i).c_str());
		}
		cout << endl;
	}
}

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}

int getLastId(void* notUsed, int argc, char** argv, char** azCol)
{
	cout << "the Last id is: " << argv[0] << endl;
	return 0;
}


int main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	bool flag = true;


	rc = sqlite3_open("FirstPart.db", &db);

	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return 1;
	}
	system("Pause");
	system("CLS");
	cout << "create table people(id integer primary key autoincrement, name string)" << endl;

	


	rc = sqlite3_exec(db, "create table people(id integer primary key autoincrement, name string)", callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	system("Pause");
	system("CLS");
	cout << "insert into people(name) values('col')" << endl;
	if (flag)
	{
		rc = sqlite3_exec(db, "insert into people(name) values('col')", NULL, 0, &zErrMsg);

		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}
	}
	system("Pause");
	system("CLS");

	cout << "insert into people(name) values('paul')" << endl;
	if (flag)
	{
		rc = sqlite3_exec(db, "insert into people(name) values('paul')", NULL, 0, &zErrMsg);

		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}
	}
	system("Pause");
	system("CLS");

	cout << "insert into people(name) values('nar')" << endl;
	if (flag)
	{
		rc = sqlite3_exec(db, "insert into people(name) values('nar')", NULL, 0, &zErrMsg);

		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}
	}
	system("Pause");
	system("CLS");

	cout << "update Students set name='ter' where id=3" << endl;
	if (flag)
	{
		rc = sqlite3_exec(db, "update people set name='ter' where id=3", NULL, 0, &zErrMsg);

		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}
	}
	system("Pause");
	system("CLS");

	rc = sqlite3_exec(db, "select * from people", callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else
	{
		printTable();
	}
	system("Pause");
	system("CLS");

	sqlite3_close(db);
	return 0;
}
