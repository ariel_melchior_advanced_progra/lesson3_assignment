#include "sqlite3.h"
#include <iostream>
#include <sstream>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

unordered_map<string, vector<string>> results;
void clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

void printTable()
{
	auto iter = results.end();
	iter--;
	//int size = results.begin()->second.size();
	int size = iter->second.size();
	size = results.begin()->second.size();
	for (int i = -2; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			if (i == -2)
			{
				//cout << it->first << " ";
				printf("|%*s|", 13, it->first.c_str());
			}
			else if (i == -1)
				cout << "_______________";
			else
				//	cout << it->second.at(i) << " ";
				printf("|%*s|", 13, it->second.at(i).c_str());
		}
		cout << endl;
	}
}

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}

int getLastId(void* notUsed, int argc, char** argv, char** azCol)
{
	cout << "the Last id is: " << argv[0] << endl;
	return 0;
}

bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	int rc;
	int availabe;
	int price;
    int balance;
	clearTable();
	std::stringstream s;
	s << "select available from cars where id="<<carid << endl;


	string str = s.str();
	
	rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else
	{
		for (auto& x : results) {
			availabe = atoi(x.second.back().c_str());
		}
	

	}
	system("Pause");
	system("CLS");
	clearTable();
	s.str(std::string());
	s << "select price from cars where id=" << carid << endl;


	str = s.str();

	rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else
	{
		for (auto& x : results) {
			price = atoi(x.second.back().c_str());
		}
		
	}
	system("Pause");
	system("CLS");
	clearTable();
	s.str(std::string());
	s << "select balance from accounts where id=" << buyerid << endl;


	str = s.str();

	rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else
	{
		for (auto& x : results) {
			balance = atoi(x.second.back().c_str());
		}
		
	}
	system("Pause");
	system("CLS");
	clearTable();
	if (availabe == 1 && balance >= price)
	{
		
		s.str(std::string());
		s << "update accounts set balance ="<<balance-price<<" where id=" << buyerid << endl;


		str = s.str();

		rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);

		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}
		s.str(std::string());
		s << "update cars set available = 0 where id=" << carid << endl;


		str = s.str();

		rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);

		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}
		return true;

	}
	else
	{
		return false;
	}
}

bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	int rc;
	int balanceFrom;
	
	std::stringstream s;

	s.str(std::string());
	s << "select balance from accounts where id=" << from << endl;


	string str = s.str();

	rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else
	{
		for (auto& x : results) {
			balanceFrom = atoi(x.second.back().c_str());
		}

	}
	system("Pause");
	system("CLS");
	clearTable();
	if (balanceFrom >= amount)
	{
		sqlite3_exec(db, "BEGIN TRANSACTION;", NULL, NULL, NULL);
		s.str(std::string());
		s << "update accounts set balance = balance + "<< amount <<" where id=" << to << endl;


		str = s.str();

		rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);

		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}
		s.str(std::string());
		s << "update accounts set balance = balance - "<<amount<<" where id=" << from << endl;


		str = s.str();

		rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);

		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}
		sqlite3_exec(db, "END TRANSACTION;", NULL, NULL, NULL);
		return true;
	}
	else
	{
		return false;
	}
}


int main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	bool flag = true;
	bool work = false;


	rc = sqlite3_open("carsDealer.db", &db);

	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return 1;
	}
	system("Pause");
	system("CLS");

	work = carPurchase(11, 9, db, zErrMsg);
	if (work)
	{
		cout <<"true" << endl;
	}

	else
	{
		cout << "false" << endl;
	}
	
	
	work = carPurchase(12, 1, db, zErrMsg);
	if (work)
	{
		cout << "true" << endl;
	}
	else
	{
		cout << "false" << endl;
	}

	work = carPurchase(12, 8, db, zErrMsg);
	if (work)
	{

		cout << "true" << endl;
	}
	else
	{
		cout << "false" << endl;
	}
	work = balanceTransfer(12, 3, 20000, db, zErrMsg);
	if (work)
	{

		cout << "true" << endl;
	}
	else
	{
		cout << "false" << endl;
	}
	
	system("pause");

}

